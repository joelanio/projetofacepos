/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package resources;

import entidades.UserFb;
import fachada.FacebookManager;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;

/**
 * REST Web Service
 *
 * @author Joew
 */
@Path("/rest")
public class RestResources {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of RestResources
     */
    public RestResources() {
    }

    @GET
    @Path(value = "/usuario/{token}")
    @Produces("text/xml")
    public UserFb getUsuario(@PathParam("token")String token) {
        FacebookManager fm = new FacebookManager();
        return fm.getUser(token);
    }
    

}
