/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fachada;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.exception.FacebookException;
import com.restfb.types.User;
import entidades.UserFb;

/**
 *
 * @author Joew
 */
public class FacebookManager {
    public UserFb getUser(String token){
        User faceUser;
        try{
            FacebookClient fc = new DefaultFacebookClient(token);
            faceUser = fc.fetchObject("me", User.class);
        }catch(FacebookException e){
               return null;
            }
        UserFb appUser = new UserFb();
        appUser.setName(faceUser.getUsername());
        appUser.setToken(token);
        appUser.setUid(faceUser.getId());
        return appUser;
  
    }
}
