/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente_rest;

import entidades.UserFb;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

/**
 * Jersey REST client generated for REST resource:RestResources [/rest]<br>
 * USAGE:
 * <pre>
 *        ClienteRest client = new ClienteRest();
 *        Object response = client.XXX(...);
 *        // do whatever with response
 *        client.close();
 * </pre>
 *
 * @author Joew
 */
@ManagedBean(name = "cliente")
@ViewScoped

public class ClienteRest implements Serializable{
    private UserFb usuario;
    private WebTarget webTarget;
    private Client client;
    private static final String BASE_URI = "http://localhost:8080/POS_AutenticaFACE/resources";

    public ClienteRest() {
        client = javax.ws.rs.client.ClientBuilder.newClient();
        webTarget = client.target(BASE_URI).path("rest");
        usuario = new UserFb();
    }

    public String login() throws ClientErrorException {
        WebTarget resource = webTarget;
        resource = resource.path(java.text.MessageFormat.format("usuario/{0}", new Object[]{getUsuario().getToken()}));
        setUsuario(resource.request(javax.ws.rs.core.MediaType.TEXT_XML).get(UserFb.class));
        if(usuario!=null) return "home";
        else return "erro_login";
    }

    public void close() {
        client.close();
    }

    /**
     * @return the usuario
     */
    public UserFb getUsuario() {
        return usuario;
    }

    /**
     * @param usuario the usuario to set
     */
    public void setUsuario(UserFb usuario) {
        this.usuario = usuario;
    }
    
}
